/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xnotify.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import ru.pixelsky.xnotify.XNotify;

import java.nio.charset.Charset;

/**
 *
 * @author Smile
 */
public class CommandListener implements CommandExecutor {
    private static final Charset charset = Charset.forName("UTF-8");

    private Plugin plugin;

    public CommandListener(Plugin p)
    {
        this.plugin = p;
    }
    
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) 
    {
        if(cmd.getName().equalsIgnoreCase("notify"))
        {   
            if(!sender.hasPermission("xnotify.send"))
            {
                sender.sendMessage(ChatColor.RED + "У вас нет прав для этого!");
                return true;
            }
            
            Player player = Bukkit.getServer().getPlayer(args[0]);
            
            if(!args[0].equalsIgnoreCase("-l") && !args[0].equalsIgnoreCase("-h"))
            {
                if(player == null)
                {
                    sender.sendMessage(ChatColor.RED + args[0] +"'s не онлайн!");
                    return true;
                }
            }
            
            StringBuilder message = new StringBuilder();
            
            if (args.length > 0)
            {
                if(args[0].equalsIgnoreCase("-l"))
                {
                    message.append(args[1]);
                
                    for (int i = 2; i < args.length; i++) 
                    {
                        message.append(" ");
                        message.append(args[i]);
                    }
                    for(Player p: Bukkit.getOnlinePlayers())
                        p.sendPluginMessage(plugin, XNotify.CHANNEL_NOTIFY_LOW, message.toString().getBytes(charset));
                    return true;
                }
                else if(args[0].equalsIgnoreCase("-h"))
                {
                    message.append(args[1]);
                
                    for (int i = 2; i < args.length; i++) 
                    {
                        message.append(" ");
                        message.append(args[i]);
                    }   
                    
                    for(Player p: Bukkit.getOnlinePlayers())
                        p.sendPluginMessage(plugin, XNotify.CHANNEL_NOTIFY_HIGHT, message.toString().getBytes(charset));
                    return true;
                }
                else if(player.isOnline())
                {
                    message.append(args[1]);
                
                    for (int i = 2; i < args.length; i++) 
                    {
                        message.append(" ");
                        message.append(args[i]);
                    }
                    
                    player.sendPluginMessage(plugin, XNotify.CHANNEL_NOTIFY_LOW, message.toString().getBytes(charset));
                    return true;
                }
                else 
                {
                    message.append(args[0]);
                
                    for (int i = 1; i < args.length; i++) 
                    {
                        message.append(" ");
                        message.append(args[i]);
                    }
                    for(Player p: Bukkit.getOnlinePlayers())
                        p.sendPluginMessage(plugin, XNotify.CHANNEL_NOTIFY_LOW, message.toString().getBytes(charset));
                    return true;
                }
            }
            else 
            {
                sender.sendMessage(ChatColor.RED + "Использование: /notify <-l|-h> <message>");
                return true;
            }
        }
        return true;
    }
}
