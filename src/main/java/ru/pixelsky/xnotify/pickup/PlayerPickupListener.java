/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xnotify.pickup;

import me.dpohvar.powernbt.nbt.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import ru.pixelsky.xnotify.XNotify;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PlayerPickupListener implements Listener {
    public static final String XMOD_PICKUP_CHANNEL = "XMod|Pickup";

    private XNotify plugin;

    public PlayerPickupListener(XNotify plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemPickup(PlayerPickupItemEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItem().getItemStack();

        NBTTagCompound nbt = itemStack2Compound(itemStack);
        byte[] bytes = nbt2Bytes(nbt);

        player.sendPluginMessage(plugin, XMOD_PICKUP_CHANNEL, bytes);
    }

    private NBTTagCompound itemStack2Compound(ItemStack is) {
        NBTContainerItem container = new NBTContainerItem(is);
        NBTTagCompound tag = container.getTag();

        NBTTagCompound isCompound = new NBTTagCompound();
        isCompound.setName("item");
        isCompound.set("id", new NBTTagShort((short) is.getTypeId()));
        isCompound.set("Count", new NBTTagByte((byte) is.getAmount()));
        isCompound.set("Damage", new NBTTagShort(is.getDurability()));

        if (tag != null)
            isCompound.set("tag", tag);

        return isCompound;
    }

    private byte[] nbt2Bytes(NBTBase tag) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);

            dos.writeByte(tag.getTypeId());
            dos.writeUTF(tag.getName());
            tag.write(dos);

            return baos.toByteArray();
        } catch (IOException e) {
            Bukkit.getLogger().info("You will never see me");
            return null;
        }
    }
}
