/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xnotify;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import ru.pixelsky.xnotify.commands.CommandListener;
import ru.pixelsky.xnotify.pickup.PlayerPickupListener;

/**
 *
 * @author Smile
 */
public class XNotify extends JavaPlugin implements Listener
{
    public static final String CHANNEL_NOTIFY_LOW = "XMod|Notify_LOW";
    public static final String CHANNEL_NOTIFY_HIGHT = "XMod|Notify_HIGH";
    
    @Override
    public void onEnable()
    {
        getServer().getMessenger().registerOutgoingPluginChannel(this, CHANNEL_NOTIFY_LOW);
        getServer().getMessenger().registerOutgoingPluginChannel(this, CHANNEL_NOTIFY_HIGHT);
        getServer().getMessenger().registerOutgoingPluginChannel(this, PlayerPickupListener.XMOD_PICKUP_CHANNEL);
        getServer().getPluginManager().registerEvents(new PlayerPickupListener(this), this);
        CommandListener c = new CommandListener(this);
        getCommand("notify").setExecutor(c);
    }
}
